#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "DatosMemCompartida.h"
#include <sys/mman.h>


int main(){
	DatosMemCompartida * data;
	int f;
        if((f=open("/tmp/fich_memoria", O_RDWR))<0){
		perror("Error en apertura fichero");
		exit(1);
	}
        if((data=(DatosMemCompartida*)mmap(NULL,sizeof(DatosMemCompartida),
        PROT_READ|PROT_WRITE, MAP_SHARED, f, 0))==(void*)MAP_FAILED){
		perror("Error en la proyección del fichero");
		close(f);
		exit(1);
	}

        close(f);
	
	while(1){
	usleep(25000);	
	if(data->esfera.centro.y > data->raqueta1.y1)
			data->accion=1;
		else if(data->esfera.centro.y < data->raqueta1.y2)
			data->accion=-1;
		else data->accion=0;
	}

}
