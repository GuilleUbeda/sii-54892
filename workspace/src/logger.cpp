#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <iostream>

int main(){
	int fd;
	char datos2[2];
	if(mkfifo("/tmp/fifo",0666)<0){
		perror("No puede crearse fifo");
		exit(1);
	}
	if((fd=open("/tmp/fifo",O_RDWR))<0){//se abre en modo lectura
		perror("No puede abrirse fifo en logger.cpp");
		exit(1);
	}
	while(1){
	read(fd,&datos2,sizeof(datos2));
	std::cout<<"Jugador "<<datos2[0]<<" marca 1 punto, lleva un total de "<<datos2[1]<<" puntos."<<std::endl;
	}
	close(fd);
	unlink("/tmp/fifo");
}
