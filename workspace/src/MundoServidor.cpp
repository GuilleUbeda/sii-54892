// MundoServidor.cpp: implementation of the CMundoServidor class.
//HECHO POR: GUILLERMO UBEDA
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"
#include <pthread.h>


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundoServidor::CMundoServidor()
{
	Init();
}

CMundoServidor::~CMundoServidor()
{
	close(fd);     //cierre fifo logger
	close(fd_com); //cierre fifo com serv-cl
	close(fd_thr); //cierre fifo thread
}

void CMundoServidor::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoServidor::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoServidor::OnTimer(int value)
{	
	char datos[2];
	//memor->esfera=esfera;
	//memor->raqueta1=jugador1;
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);

	//switch(memor->accion){
	//case -1:CMundoServidor::OnKeyboardDown('s',0,0);break;
	//case 1 :CMundoServidor::OnKeyboardDown('w',0,0);break;
	//}


	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		if(puntos2==3){
			printf("Fin del juego. Ganador: Jugador 2");
			exit(0);
		}
		datos[0]='2';
		datos[1]=puntos2+48;
		//printf("escritura jugador1\n");
		write(fd, &datos, sizeof(datos));
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		if(puntos1==3){
                        printf("Fin del juego. Ganador: Jugador 1");
                        exit(0);
                }
		datos[0]='1';
		datos[1]=puntos1+48; //suma de ints para dar simbolo asci
		//printf("escritura jugador2\n");
		write(fd, &datos, sizeof(datos));
		if(esfera.radio>=0.2)
		esfera.radio-=0.1;
	}

	//actualizacion atributos
	char cad_servcl[200];
	sprintf(cad_servcl, "%f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x,
	esfera.centro.y, jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,
	jugador2.y1,jugador2.x2,jugador2.y2, puntos1, puntos2);

	//escritura fifocom
	write(fd_com, &cad_servcl, sizeof(cad_servcl));
}

void CMundoServidor::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}
}

void* hilo_comandos(void* d)
{
      CMundoServidor* p=(CMundoServidor*) d;
      p->RecibeComandosJugador();
}

void CMundoServidor::Init()
{
	//memoria.esfera=esfera;
	//memoria.raqueta1=jugador1;
	//memoria.accion=0;
	
	if((fd=open("/tmp/fifo", O_WRONLY))<0){
		perror("No puede abrirse Fifo en MundoServidor.cpp");
		exit(1);
	}

	//apertura tuberia coms servidor-cliente
        if((fd_com=open("/tmp/fifocom", O_WRONLY))<0){
                perror("No puede abrirse fifocom en MundoServidor.cpp");
                exit(1);
        }

	//apertura tuberia coms cliente-servidor(thread)
	if((fd_thr=open("/tmp/fifothread", O_RDONLY))<0){
		perror("No puede abrirse fifothread en MundoServidor.cpp");
		exit(1);
	}

	//creacion thread
	pthread_create(&thid1, NULL, hilo_comandos, this);

	//int f;
	//if((f=open("fich_memoria", O_RDWR|O_CREAT|O_TRUNC, 0666))<0){
	//	perror("No puede abrirse fich_memoria");
	//	exit(1);
	//}
	//write(f,&memoria, sizeof(DatosMemCompartida));
	//if((memor=(DatosMemCompartida*)mmap(NULL,sizeof(DatosMemCompartida),
	//PROT_READ|PROT_WRITE, MAP_SHARED, f, 0))==(void*)MAP_FAILED){
	//	perror("Error en la proyeccion del archivo f");
	//	close(f);
	//	exit (1);
	//}
	//close(f);

	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}

void CMundoServidor::RecibeComandosJugador()
{
     while (1) {
            usleep(10);
            char cad_clserv[100];
            read(fd_thr, &cad_clserv, sizeof(cad_clserv));
            unsigned char key;
            sscanf(cad_clserv,"%c",&key);
            if(key=='s')jugador1.velocidad.y=-4;
            if(key=='w')jugador1.velocidad.y=4;
            if(key=='l')jugador2.velocidad.y=-4;
            if(key=='o')jugador2.velocidad.y=4;
      }
}
